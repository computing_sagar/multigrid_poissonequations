#This is Assignment 1 for Simulation and Scientific Computing .
#This program solve pure poisson’s Equation with Dirichlet and Neumann Boundary Condition.

//*************RUNNING THE PROGRAM **************************//

To Run the Program correctly , please follow the following steps 

Step 1 : Compile the program either by typing in <./simulation.sh> or by typing in <chmod 	+x simulation.sh> for privileges or by just typing in make all . Use the compiler 	version gcc 4.9.2 /4.9.3 

Step 2 : Once the Program has been compiled and it gives no error , executable #mgsolve# 	 will be generated and you can start the program . 

Step 2.5 : You can jump to OpemMP parallelisation and then come back again here.

Step 3 : To run the program , type in ./mgsolve <level> <number of vcycles> <Type of 		 Boundary Condition> . e.g you can run ./mgsolve 5 5 D for dirichlet case and ./	 mgsolve 5 5 N for the neumann case . You can simply type ./mgsolve 5 5 and the 	 default boundary condition will be taken i.e Dirichlet 

Step 4 : Once the program execution has been completed , you can see the residual per 		 Vcycle and the total time taken . 
	 Solution.txt will be generated which can be visualised by gnu plot . 

Step 5 : To plot the graph , open gunplot in terminal window .

Step 6 : type in <splot “Solution.txt” u 1:2:3 with points palette> to visualise the 		 output.

Step 7 : Sample output has already been provided for Dirichlet and Neumann Case in the 		 folder, you can visualise by opening the files in gnuplot 

//********************OpenMP Parallelization*************//

#This program already includes , #pragma parallel for# statement to parallelise the code by OpenMP . You can run program explicitly in parallel by setting the number of threads as follows :

step 0 : cat /proc/cpuinfo - to find out the number of cores or processors 
step 1 : set OMP_NUM_THREADS=<number of cores/processors> * 2 

# Then you can again , start the ./mgsolve by going to step 3 as mentioned above # 

//**************SAMPLE OUTPUT ****************************//

# The pdf file has already been provided the showing error vs number of levels both for Neumann and Dirichlet and also sample plots 

# Please avoid entering Negative or floating values in the input arguments , although program can take care of it , but as you know “precaution is better than cure “.

#Thank you and Enjoy Multigrid .

//*********************************************************//

    
