#Declaring the Variables
CC=g++

#Declaring the flags
CFLAGS = -c -std=c++11 -O3 -Wall -Winline -Wshadow -fopenmp
FLAGS  = -std=c++11 -O3 -Wall -Winline -Wshadow -fopenmp

all			:Multigrid

Multigrid	 	:Grid.o VcycleFormation.o Stencil.o MultigridSolver.o Time.o	Configuration.o
			$(CC) $(FLAGS) Grid.o VcycleFormation.o Stencil.o MultigridSolver.o Time.o	Configuration.o  Simulation.cpp -o mgsolve

Grid.o			:Grid.cpp
			$(CC) $(CFLAGS) Grid.cpp

VcycleFormation.o	:VcycleFormation.cpp
			$(CC) $(CFLAGS) VcycleFormation.cpp

Stencil.o		:Stencil.cpp
			$(CC) $(CFLAGS) Stencil.cpp

MultigridSolver.o	:MultigridSolver.cpp
			$(CC) $(CFLAGS) MultigridSolver.cpp
			
Time.o 			:Time.cpp
			$(CC) $(CFLAGS) Time.cpp
			
Configuration.o		:Configuration.cpp
			$(CC)	$(CFLAGS)	Configuration.cpp

clean			:
			rm -rf *o *.out Solution.txt 
