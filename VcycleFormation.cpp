//
//  VcycleFormation.cpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "VcycleFormation.hpp"

SIWIR2::MULTIGRID::VcycleFormation::VcycleFormation(const real_l _levelfine, const SIWIR2::MULTIGRID::BOUNDARY _boundaryType) : levelfine_(_levelfine), boundaryType_(_boundaryType){
    
    // Prepare Vcycle
    for (auto i = levelfine_; i >= 1; --i) {
        
        const std::shared_ptr<Grid> u_ = std::make_shared<Grid>(i,SIWIR2::MULTIGRID::GridType::Ugrid) ;
        U_.push_back(u_) ;
        
        const std::shared_ptr<Grid> f_ = std::make_shared<Grid>(i,SIWIR2::MULTIGRID::GridType::Fgrid) ;
        F_.push_back(f_) ;
        
        const std::shared_ptr<Grid> r_ = std::make_shared<Grid>(i,SIWIR2::MULTIGRID::GridType::Rgrid) ;
        R_.push_back(r_) ;
        
    }
    
    if (boundaryType_ ==0) {
        this->Initialise_Vycle_Dirichlet() ;
    }
    if (boundaryType_ == 1){
        this->Initialize_Vcycle_Neumann() ;
    }
}
SIWIR2::MULTIGRID::VcycleFormation::~VcycleFormation(){
    
}

void SIWIR2::MULTIGRID::VcycleFormation::Initialise_Vycle_Dirichlet(){
    
    assert(this->U_.at(0)->get_Grid_Type() == 0) ;
    
    real_l numGridPosX_ = this->U_.at(0)->get_Num_Grids() ;
    //std::cout<<numGridPosX_;
    real_d meshSize_ = 1.0 / (numGridPosX_-1) ;
    auto j = numGridPosX_ -1 ;
    auto y_ = j * meshSize_ ;
    
    // Initialising the boundary value for the uppermost part of the grid and this is only for Dirichlet boundary Conditions
    // #pragma omp parallel for schedule static 
    #pragma omp parallel for schedule (static)
    for (real_l i =0 ; i < numGridPosX_; ++i) {
            real_d x_ = i * meshSize_ ;
            //std::cout<<x_<<" "<<y_s<<std::endl ;
            this->U_.at(0)->operator()(i, j) = sin(SIWIR2::pie * x_) * sinh(SIWIR2::pie * y_) ;
        }
    
    std::cout<<"Vcycle constructed and Prepared"<<std::endl;
}

void SIWIR2::MULTIGRID::VcycleFormation::display_Data(const real_l level, const SIWIR2::MULTIGRID::GridType gridType){
    
    real_l pos = get_Position(level) ;
    real_l numGridPosX_ = this->U_.at(pos)->get_Num_Grids() ;
    
    for (real_l y =0 ; y < numGridPosX_; ++y) {
        for (real_l x =0 ; x < numGridPosX_; ++x ) {
            if (gridType ==0) {
                std::cout<<this->U_.at(pos)->operator()(x, y)<<std::endl;

            }
            else if (gridType ==1){
                std::cout<<this->F_.at(pos)->operator()(x, y)<<std::endl;
            }
            else
                std::cout<<this->R_.at(pos)->operator()(x, y)<<std::endl;
        }
    }
    
}

real_l SIWIR2::MULTIGRID::VcycleFormation::get_Position(const real_l _level){
    
    auto pos = this->levelfine_ - _level ;
    return (real_l) pos ;
}

void SIWIR2::MULTIGRID::VcycleFormation::write_Solution(const real_l level, const SIWIR2::MULTIGRID::GridType _gridType){
    
    std::ofstream write_ ;
    std::string name = "Solution" ;
    std::string level_ = std::to_string(level) ;
    std::string type_ = std::to_string(_gridType) ;
    std::string filename(name+".txt") ;
    std::string folder("Grid/" + filename) ;
    
    auto pos = get_Position(level) ;
    //real_l numtotal = this->U_[pos]->get_Num_Grids();
    write_.open(filename) ;
    //std::cout<<numtotal<<std::endl;
    
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;
    
    //std::cout<<N_x<<std::endl;
    //std::cout<<N_y<<std::endl;
    
    real_d meshsize = 1.0 / (1 << level) ;
    
    real_d offset = 0.0 ;
    if (boundaryType_==0) {
        offset = 0.0 ;
    }
    if (boundaryType_== 1) {
        offset = meshsize ;
    }
    
    for (real_l y = 0 ; y < N_y; ++y) {
        for (real_l x =0 ; x < N_x; ++x) {
            real_d xcord = x * meshsize - offset;
            real_d ycord = y * meshsize  -0.0 ;
            if (_gridType==0) {
                write_<<xcord<<" "<<ycord<<" "<<U_[pos]->operator()(x, y)<<std::endl ;
            }
            else if (_gridType ==1)
                write_<<xcord<<" "<<ycord<<" "<<F_[pos]->operator()(x, y)<<std::endl ;
            else
                write_<<xcord<<" "<<ycord<<" "<<R_[pos]->operator()(x, y)<<std::endl ;

        }
    }
}

void SIWIR2::MULTIGRID::VcycleFormation::allocate_Ghost_Points(){
    
    // Allocating for U's
    for (auto iter = U_.begin(); iter!= U_.end(); ++iter) {
        //auto n_d =  2 * (**iter).get_Num_Grids() ;
        // We will allocate ghost Pos
        (**iter).resize() ;
        
    }
    // Allocating for F's
    for (auto iter = F_.begin(); iter!= F_.end(); ++iter) {
        //auto n_d = 2 * (**iter).get_Num_Grids() ;
        // We will allocate ghost Pos
        (**iter).resize() ;
        
    }
    // Allocating for R's
    for (auto iter = R_.begin(); iter!= R_.end(); ++iter) {
        //auto n_d = 2 * (**iter).get_Num_Grids() ;
        // We will allocate ghost Pos
        (**iter).resize() ;
    }
}

void SIWIR2::MULTIGRID::VcycleFormation::neumann_Boundary_Conditions(){
    
    // This is only for Finest level Grid
    // Initialising the Top upper Geometric part only for the finest grid
    
    auto N = U_[0]->get_Num_Grids() ;
    real_d meshsize_ = 1.0 / (N-1) ;
    auto x_new = N + 2 ;
    
    #pragma omp parallel for schedule (static)
    for (real_l i = 1 ; i < x_new-1 ; ++i) {
        real_d x = i * meshsize_ - meshsize_;
        // Initialising the Top Boundary ...//
        U_[0]->operator()(i ,N-1 ) = x * (1.0 -x ) ;
        
        // Initialising the Bottom Boundary ...//
        U_[0]->operator()(i, 0) = x * (1.0 -x) ;
    }
    
}

void SIWIR2::MULTIGRID::VcycleFormation::update_Ghost_Points(const real_d gradient_){
    
    // This will evaluate Gradient at Right and Left hand side and make appropriate changes
    // Traversing the array
    
    real_d gradient = gradient_ ;

    auto N = U_[0]->get_Num_Grids() ;
    real_d meshsize_ = 1.0 / (N-1) ;
    #pragma omp parallel for schedule (static)
    for (real_l j = 0; j < N; ++j) {
        
        U_[0]->operator()(0, j) =  (gradient * meshsize_)  + ( U_[0]->operator()(1, j)) ;
        U_[0]->operator()(N+1,j) = (gradient * meshsize_) + ( U_[0]->operator()(N, j)) ;
    }
}

void SIWIR2::MULTIGRID::VcycleFormation::Initialize_Vcycle_Neumann(){
    
    allocate_Ghost_Points() ;
    neumann_Boundary_Conditions() ;
    update_Ghost_Points(-1.0) ;
}

void SIWIR2::MULTIGRID::VcycleFormation::Initialise_F(){


}






