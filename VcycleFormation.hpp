//
//  VcycleFormation.hpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef VcycleFormation_hpp
#define VcycleFormation_hpp

#include <stdio.h>
#include <iostream>
#include <utility>
#include <memory>
#include <assert.h>
#include <fstream>
#include <string>
#include "Grid.hpp"
#include <omp.h>

//typedef  SIWIR2::MULTIGRID::GridType GridType_;

namespace SIWIR2 {
    namespace MULTIGRID {
        
        enum BOUNDARY {
            
            Dirichlet ,
            Neumann
        };
        
        class VcycleFormation{            
        public:
            
            const real_l levelfine_ ;
            const SIWIR2::MULTIGRID::BOUNDARY boundaryType_   ;
            
            std::vector< std::shared_ptr< Grid >> U_ ;
            std::vector< std::shared_ptr< Grid >> R_ ;
            std::vector< std::shared_ptr< Grid >> F_ ;
            
            VcycleFormation(const real_l _levelfine,const SIWIR2::MULTIGRID::BOUNDARY _boundary) ;
            ~VcycleFormation() ;
            
            void Initialise_Vycle_Dirichlet() ;
            void Initialize_Vcycle_Neumann() ;
            void Initialise_F() ; 
            void write_Solution(const real_l level,const SIWIR2::MULTIGRID::GridType _gridType) ;
            real_l get_Position(const real_l _level) ;
            
            // Display function
            void display_Data(const real_l level,const SIWIR2::MULTIGRID::GridType gridType) ;
            
            // Neumann Boundary Conditions
            void allocate_Ghost_Points() ;
            void neumann_Boundary_Conditions() ;
            void update_Ghost_Points(const real_d gradient_) ;
        };
        
        
    }
}

#endif /* VcycleFormation_hpp */
