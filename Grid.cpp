//
//  Grid.cpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Grid.hpp"

SIWIR2::MULTIGRID::Grid::Grid(const real_l _level, const MULTIGRID::GridType _type) : level_(_level), gridType_(_type), numGridPointsT_(((1<<level_) +1) * ((1<<level_)+1)), numGridpointsX_(((1<<level_) +1) ){
    
    //this->meshsize_ = 1.0 / (numGridpointsX_ -1 ) ;
    this->allocate() ;

}

SIWIR2::MULTIGRID::Grid::~Grid(){
    
}
void SIWIR2::MULTIGRID::Grid::allocate() {
    
    this->data.resize(numGridPointsT_, 0.0) ;

}

const real_d& SIWIR2::MULTIGRID::Grid::operator()(const real_l x_dir, const real_l y_dir) const {
    
    unsigned long n = getNx() ;
    return data[ y_dir * n + x_dir ] ;

}

real_d& SIWIR2::MULTIGRID::Grid::operator()(const real_l x_dir, const real_l y_dir) {
    
    unsigned long n = getNx() ;
    return data[y_dir * n + x_dir] ;
    
}

const SIWIR2::MULTIGRID::GridType SIWIR2::MULTIGRID::Grid::get_Grid_Type() const{
    
    return gridType_ ;
    
}

const real_l SIWIR2::MULTIGRID::Grid::get_Num_Grids() const {
    
    return numGridpointsX_ ;
    
}

const real_l SIWIR2::MULTIGRID::Grid::get_Total_Num_Grids() const {
    return numGridPointsT_ ;
}

void SIWIR2::MULTIGRID::Grid::reset(){
    
    data.clear() ;
    data.resize(numGridPointsT_, 0.0) ;
    
}

void SIWIR2::MULTIGRID::Grid::reset(const unsigned long size_){
    
    data.clear() ;
    data.resize(size_, 0.0) ; 
    
}

void SIWIR2::MULTIGRID::Grid::resize(real_d num){
    
    unsigned long number = (2 * numGridpointsX_) + numGridPointsT_ ;
    data.resize(number, num) ;
}

void SIWIR2::MULTIGRID::Grid::resize(const unsigned long size_, const real_d num){
    
    data.resize(size_, num) ;
    
}

const real_l SIWIR2::MULTIGRID::Grid::get_Size() const {
    
    return data.size() ;
}

real_l SIWIR2::MULTIGRID::Grid::getNx() const {
    
    real_l x_coloumn = data.size() / numGridpointsX_ ;
    return x_coloumn ;

}

real_l SIWIR2::MULTIGRID::Grid::getNy() const {
    
    return ( numGridpointsX_ )  ;
}

std::vector<real_d>::const_iterator SIWIR2::MULTIGRID::Grid::getbegin() const {

    return data.begin() ;
}

std::vector<real_d>::const_iterator SIWIR2::MULTIGRID::Grid::getlast() const {
    return data.end() ;
}




